# Ansible - AnsiblR - Basics Demo for LinuxTag 2019

## Installation
Best way is to use Python pip!

~~~
sudo apt update && sudo apt install python-pip git
sudo pip install ansible
~~~

## Examples
Checkout the examples via Git
~~~
git clone https://gitlab.com/n0r1sk/linuxday/ansible-r-basics.git
~~~
